set encoding=utf-8
call plug#begin('~/.vim/plugged')
Plug 'sophacles/vim-bundle-mako'
Plug 'tpope/vim-dispatch'
Plug 'airblade/vim-gitgutter'
Plug 'vim-airline/vim-airline'
Plug 'BeneCollyridam/futhark-vim'
Plug 'vim-syntastic/syntastic'
Plug 'tpope/vim-commentary'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'vmchale/futhark-syntastic-vim'
Plug 'scrooloose/nerdtree'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-fugitive'
Plug 'eigenfoo/stan-vim'
Plug 'valloric/youcompleteme', { 'do': './install.sh --clang-complete' }
call plug#end()

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_fut_checkers = ['futhark']

let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']

nnoremap <SPACE> <Nop>
set hidden wildmenu
let mapleader=" "
map <leader>bl :b#<cr>
map <leader>w <c-w>
map <leader>. :Ex<cr>

" FZF: find buffers, files, in files, lines
map <leader>bb :Buffers<cr>
map <leader>of :FZF<cr>
map <leader>ff :Rg<space>
map <leader>ff :Lines<cr>

" comment/uncomment
map <leader>lc gcc

" git stuff
map <leader>gg :Git<cr>
map <leader>gc :Git commit<cr>
map <leader>gp :Git push<cr>
map <leader>ga :w<cr>:Git add <c-r>%<cr>

" vim config
map <leader>ve :e ~/.vim/vimrc<cr>
map <leader>vr :so ~/.vim/vimrc<cr>
map <leader>vp :PlugInstall<cr>

" color that docker stuff
autocmd BufNewFile,BufRead *.dockerfile set ft=dockerfile

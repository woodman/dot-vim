FROM ubuntu:20.04
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y python3-pip vim cmake git
RUN cd $HOME \
 && git clone https://github.com/maedoc/.vim \
 && vim -c PlugInstall -c qa
